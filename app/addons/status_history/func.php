<?php

use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

function fn_status_history_change_order_status_post(&$order_id, &$status_to, &$status_from, $force_notification, $place_order, $order_info, $edp_data)
{
    $change_date = time();
    $user_id = Tygh::$app['session']['auth']['user_id'];

    $history_data = [
        'order_id' => $order_id,
        'old_status' => $status_from,
        'new_status' => $status_to,
        'date' => $change_date,
        'user_id' => $user_id
    ];

    $add_history = db_query('INSERT INTO ?:status_history ?e', $history_data);
}

function fn_get_status_history(array $params, int $items_per_page = 0): array
{
    $sorthing_default_params = [
        'page' => 1,
        'items_per_page' => $items_per_page,
        'total_items' => 10,
        'sort_by' => 'date'
    ];

    $sortings = [
        'order_id' => 'order_id',
        'date' => 'date',
        'user_id' => 'user_id',
        'new_status' => 'new_status',
        'old_status' => 'old_status'
    ];

    $params = array_merge($sorthing_default_params, $params);
    $params['total_items'] = db_get_field("SELECT COUNT(?:status_history.id) FROM ?:status_history WHERE ?i = ?i", 1, 1);

    $sorting = db_sort($params, $sortings, 'order_id', 'desc');
    $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);

    $order_statuses_list = fn_get_simple_statuses(STATUSES_ORDER, true, true);
    $histories_list = db_get_array("SELECT * FROM ?:status_history $sorting $limit");
    $order_history_list = [];

    foreach ($histories_list as $change_history) {
        $change_history['old_status']  = $order_statuses_list[$change_history['old_status']];
        $change_history['new_status'] =  $order_statuses_list[$change_history['new_status']];
        $change_history['user_name']  = fn_get_user_name($change_history['user_id']);
        $order_history_list[] = $change_history;
    }

    return [$order_history_list, $params];
}
