<?php


if (!defined('BOOTSTRAP')) { die('Access denied'); }

use Tygh\Tygh;
use Tygh\Registry;

if($mode == 'manage'){
    $params = $_REQUEST;

    list($status_history_list, $search) = fn_get_status_history($params, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign([
        'status_history_list' => $status_history_list,
        'search' => $search
    ]);
}