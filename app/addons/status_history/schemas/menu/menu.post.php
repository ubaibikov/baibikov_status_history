<?php

$schema['central']['orders']['items']['status_history']  = [
    'attrs' => [
        'class' => 'is-addon'
    ],
    'href' => 'status_history.manage',
    'position' => 100
];


return $schema;
