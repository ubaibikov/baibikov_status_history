{capture name="mainbox"}
<form action="{""|fn_url}" method="post"  name="order_status_form" class="cm-hide-inputs">
<input type="hidden" name="fake" value="1" />

{$c_url = $config.current_url|fn_query_remove:"sort_by":"sort_order"}
{$c_icon = "<i class=\"icon-`$search.sort_order_rev`\"></i>"}
{$c_dummy = "<i class=\"icon-dummy\"></i>"}

{$rev = $smarty.request.content_id|default:"pagination_contents"}
{include file="common/pagination.tpl" save_current_page=true}
<div class="items-container" id="status_history">
    {if $status_history_list}
        <table class="table table-middle table-responsive">
            <thead>
            <tr>
                <th><a class="cm-ajax" href="{"`$c_url`&sort_by=order_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("id")}{if $search.sort_by == "order_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"`$c_url`&sort_by=old_status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status_history_old_status")}{if $search.sort_by == "old_status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"`$c_url`&sort_by=new_status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status_history_new_order_status")}{if $search.sort_by == "new_status"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"`$c_url`&sort_by=user_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status_history_who_changed")}{if $search.sort_by == "user_id"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="right"><a class="cm-ajax" href="{"`$c_url`&sort_by=date&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id={$rev}>{__("status_history_date")}{if $search.sort_by == "date"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
            </tr>
            </thead>
                <tbody>
                    {foreach $status_history_list as $history}
                        <tr class="cm-row-status">
                            <td data-th="{__("order_id")}">{$history.order_id}</td>
                            <td data-th="{__("old_status")}" {if !$history.old_status}style="color:#8bca39"{/if}>{if $history.old_status}{$history.old_status}{/if}</td>
                            <td data-th="{__("new_order_status")}" {if !$history.new_status}style="color:#b63a21"{/if}>{if $history.new_status}{$history.new_status}{/if}</td> 
                            <td data-th="{__("who_changed")}"><a  class="row-status" {if $history.user_name}href="{"profiles.update&user_id=`$history.user_id`&user_type=A"|fn_url}"{/if}>{if $history.user_name}{$history.user_name}{/if}</a></td>
                            <td class="right" data-th="{__("date")}">{$history.date|date_format:"`$settings.Appearance.date_format`"}</td>
                        </tr>
                    {/foreach}
                </tbody>
            </tr>
        </table>
    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}
<!--status_history--></div>

{include file="common/pagination.tpl" div_id=$smarty.request.content_id}

{/capture}
{include 
    file="common/mainbox.tpl"
    title=__("status_history")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    sidebar=$smarty.capture.sidebar
}
